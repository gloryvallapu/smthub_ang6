import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

import { PageHeaderModule } from './../shared/modules/page-header/page-header.module';

import { HeaderComponent } from './components/header/header.component';
import { DealerComponent } from './admin/dealer/dealer.component';
import { SupplierComponent } from './admin/supplier/supplier.component';
import { ServiceCenterComponent } from './admin/service-center/service-center.component';
import { UsersListComponent } from './admin/users-list/users-list.component';
import { ProductListComponent } from './admin/product-list/product-list.component';
import { PurchaseOrderHistoryComponent } from './admin/purchase-order-history/purchase-order-history.component';
import { WarehouseListComponent } from './admin/warehouse-list/warehouse-list.component';
import { WhProductInvoiceComponent } from './admin/wh-product-invoice/wh-product-invoice.component';
import { ShopOrderHistoryComponent } from './admin/shop-order-history/shop-order-history.component';
import { ShopInvoiceHistoryComponent } from './admin/shop-invoice-history/shop-invoice-history.component';
import { WhBarcodeComponent } from './admin/wh-barcode/wh-barcode.component';
import { ShopListComponent } from './admin/shop-list/shop-list.component';
import { ShopUsersComponent } from './admin/shop-users/shop-users.component';
import { FairContactComponent } from './admin/fair-contact/fair-contact.component';
import { AddbannerComponent } from './admin/addbanner/addbanner.component';
import { CollectionsComponent } from './admin/collections/collections.component';
import { CreateBrandComponent } from './admin/create-brand/create-brand.component';
import { FooterSetupComponent } from './admin/footer-setup/footer-setup.component';
import { HeaderSetupComponent } from './admin/header-setup/header-setup.component';
import { DiscountComponent } from './admin/discount/discount.component';
import { BillingInfoComponent } from './admin/billing-info/billing-info.component';
import { BankConfigComponent } from './admin/bank-config/bank-config.component';
import { CompanyInfoComponent } from './admin/company-info/company-info.component';
import { HeaderInfoComponent } from './admin/header-info/header-info.component';
import { MasterProductsComponent } from './admin/master-products/master-products.component';
import { TaxNdiscConfigComponent } from './admin/tax-ndisc-config/tax-ndisc-config.component';
import { ServiceTaxConfigComponent } from './admin/service-tax-config/service-tax-config.component';
import { UserTypeComponent } from './admin/user-type/user-type.component';
import { CountriesComponent } from './admin/countries/countries.component';
import { MasterBrandConfigComponent } from './admin/master-brand-config/master-brand-config.component';
import { PromotionsComponent } from './admin/promotions/promotions.component';
import { PriceListComponent } from './admin/price-list/price-list.component';
import { PaginationComponent } from './admin/pagination/pagination.component';
import { ReviewVerificationComponent } from './admin/review-verification/review-verification.component';



@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        PageHeaderModule,
        NgbDropdownModule,
        NgbModule.forRoot()
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, DealerComponent, SupplierComponent, ServiceCenterComponent, UsersListComponent, ProductListComponent, PurchaseOrderHistoryComponent, WarehouseListComponent, WhProductInvoiceComponent, ShopOrderHistoryComponent, ShopInvoiceHistoryComponent, WhBarcodeComponent, ShopListComponent, ShopUsersComponent, FairContactComponent, AddbannerComponent, CollectionsComponent, CreateBrandComponent, FooterSetupComponent, HeaderSetupComponent, DiscountComponent, BillingInfoComponent, BankConfigComponent, CompanyInfoComponent, HeaderInfoComponent, MasterProductsComponent, TaxNdiscConfigComponent, ServiceTaxConfigComponent, UserTypeComponent, CountriesComponent, MasterBrandConfigComponent, PromotionsComponent, PriceListComponent,PaginationComponent, ReviewVerificationComponent]
})
export class LayoutModule { }
