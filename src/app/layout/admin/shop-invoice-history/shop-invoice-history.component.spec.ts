import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopInvoiceHistoryComponent } from './shop-invoice-history.component';

describe('ShopInvoiceHistoryComponent', () => {
  let component: ShopInvoiceHistoryComponent;
  let fixture: ComponentFixture<ShopInvoiceHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopInvoiceHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopInvoiceHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
