import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhBarcodeComponent } from './wh-barcode.component';

describe('WhBarcodeComponent', () => {
  let component: WhBarcodeComponent;
  let fixture: ComponentFixture<WhBarcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhBarcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhBarcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
