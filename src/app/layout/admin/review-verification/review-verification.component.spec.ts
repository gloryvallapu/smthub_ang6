import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVerificationComponent } from './review-verification.component';

describe('ReviewVerificationComponent', () => {
  let component: ReviewVerificationComponent;
  let fixture: ComponentFixture<ReviewVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
