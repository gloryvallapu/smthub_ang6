import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterSetupComponent } from './footer-setup.component';

describe('FooterSetupComponent', () => {
  let component: FooterSetupComponent;
  let fixture: ComponentFixture<FooterSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
