import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhProductInvoiceComponent } from './wh-product-invoice.component';

describe('WhProductInvoiceComponent', () => {
  let component: WhProductInvoiceComponent;
  let fixture: ComponentFixture<WhProductInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhProductInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhProductInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
