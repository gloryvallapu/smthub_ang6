import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopOrderHistoryComponent } from './shop-order-history.component';

describe('ShopOrderHistoryComponent', () => {
  let component: ShopOrderHistoryComponent;
  let fixture: ComponentFixture<ShopOrderHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopOrderHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopOrderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
