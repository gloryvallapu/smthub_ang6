import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FairContactComponent } from './fair-contact.component';

describe('FairContactComponent', () => {
  let component: FairContactComponent;
  let fixture: ComponentFixture<FairContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FairContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FairContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
