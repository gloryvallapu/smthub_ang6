import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxNdiscConfigComponent } from './tax-ndisc-config.component';

describe('TaxNdiscConfigComponent', () => {
  let component: TaxNdiscConfigComponent;
  let fixture: ComponentFixture<TaxNdiscConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxNdiscConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxNdiscConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
