import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterBrandConfigComponent } from './master-brand-config.component';

describe('MasterBrandConfigComponent', () => {
  let component: MasterBrandConfigComponent;
  let fixture: ComponentFixture<MasterBrandConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterBrandConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterBrandConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
