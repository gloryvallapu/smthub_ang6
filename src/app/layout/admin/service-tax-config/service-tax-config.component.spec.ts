import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceTaxConfigComponent } from './service-tax-config.component';

describe('ServiceTaxConfigComponent', () => {
  let component: ServiceTaxConfigComponent;
  let fixture: ComponentFixture<ServiceTaxConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceTaxConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceTaxConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
