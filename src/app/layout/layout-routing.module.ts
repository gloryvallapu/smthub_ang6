import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DealerComponent } from './admin/dealer/dealer.component';
import { SupplierComponent } from './admin/supplier/supplier.component';
import { ServiceCenterComponent } from './admin/service-center/service-center.component';
import { UsersListComponent } from './admin/users-list/users-list.component';
import { ProductListComponent } from './admin/product-list/product-list.component';
import { PurchaseOrderHistoryComponent } from './admin/purchase-order-history/purchase-order-history.component';
import { ShopInvoiceHistoryComponent } from './admin/shop-invoice-history/shop-invoice-history.component';
import { ShopOrderHistoryComponent } from './admin/shop-order-history/shop-order-history.component';
import { WarehouseListComponent } from './admin/warehouse-list/warehouse-list.component';
import { WhBarcodeComponent } from './admin/wh-barcode/wh-barcode.component';
import { WhProductInvoiceComponent } from './admin/wh-product-invoice/wh-product-invoice.component';
import { ShopListComponent } from './admin/shop-list/shop-list.component';
import { ShopUsersComponent } from './admin/shop-users/shop-users.component';
import { FairContactComponent } from './admin/fair-contact/fair-contact.component';
import { AddbannerComponent } from './admin/addbanner/addbanner.component';
import { CollectionsComponent } from './admin/collections/collections.component';
import { CreateBrandComponent } from './admin/create-brand/create-brand.component';
import { FooterSetupComponent } from './admin/footer-setup/footer-setup.component';
import { HeaderSetupComponent } from './admin/header-setup/header-setup.component';
import { DiscountComponent } from './admin/discount/discount.component';
import { BillingInfoComponent } from './admin/billing-info/billing-info.component';
import { BankConfigComponent } from './admin/bank-config/bank-config.component';
import { CompanyInfoComponent } from './admin/company-info/company-info.component';
import { HeaderInfoComponent } from './admin/header-info/header-info.component';
import { MasterProductsComponent } from './admin/master-products/master-products.component';
import { TaxNdiscConfigComponent } from './admin/tax-ndisc-config/tax-ndisc-config.component';
import { ServiceTaxConfigComponent } from './admin/service-tax-config/service-tax-config.component';
import { UserTypeComponent } from './admin/user-type/user-type.component';
import { CountriesComponent } from './admin/countries/countries.component';
import { MasterBrandConfigComponent } from './admin/master-brand-config/master-brand-config.component';
import { PromotionsComponent } from './admin/promotions/promotions.component';
import { PriceListComponent } from './admin/price-list/price-list.component';
import { PaginationComponent } from './admin/pagination/pagination.component';
import { ReviewVerificationComponent } from './admin/review-verification/review-verification.component';


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'dealer', component: DealerComponent },
            { path: 'supplier', component: SupplierComponent },
            { path: 'serviceCenter', component: ServiceCenterComponent },
            { path: 'usersList', component: UsersListComponent },
            { path: 'productList', component: ProductListComponent },
            { path: 'purchaseOrderHistory', component: PurchaseOrderHistoryComponent },
            { path: 'shopInvoiceHistory', component: ShopInvoiceHistoryComponent },
            { path: 'shopOrderHistory', component: ShopOrderHistoryComponent },
            { path: 'warehouseList', component: WarehouseListComponent },
            { path: 'whbarcode', component: WhBarcodeComponent },
            { path: 'whproductInvoice', component: WhProductInvoiceComponent },
            { path: 'shopList', component: ShopListComponent },
            { path: 'shopUsers', component: ShopUsersComponent },
            { path: 'fairContact', component: FairContactComponent },
            { path: 'addBanner', component: AddbannerComponent },
            { path: 'collections', component: CollectionsComponent },
            { path: 'createBrand', component: CreateBrandComponent },
            { path: 'footerSetup', component: FooterSetupComponent },
            { path: 'headerSetup', component: HeaderSetupComponent },
            { path: 'discount', component: DiscountComponent },
            { path: 'billingInfo', component: BillingInfoComponent },
            { path: 'bankConfig', component: BankConfigComponent },
            { path: 'companyInfo', component: CompanyInfoComponent },
            { path: 'headerInfo', component: HeaderInfoComponent },
            { path: 'taxNdiscountConfig', component: TaxNdiscConfigComponent },
            { path: 'serviceTaxConfig', component: ServiceTaxConfigComponent },
            { path: 'userType', component: UserTypeComponent },
            { path: 'countries', component: CountriesComponent },
            { path: 'masterBrandConfig', component: MasterBrandConfigComponent },
            { path: 'promotions', component: PromotionsComponent },
            { path: 'priceList', component: PriceListComponent },
            { path: 'pagination', component: PaginationComponent },
            { path: 'review', component: ReviewVerificationComponent },
            { path: 'masterProducts', component: MasterProductsComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
